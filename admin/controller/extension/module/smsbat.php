<?php

class ControllerExtensionModuleSmsBat extends Controller
{
    private $_gate;
    private $_err;
    private $_log;
    private $data;

    private $message_keys = array(
        'smsbat_message_connection_error' => 'При отправке оповещения по SMS возникли неполадки со шлюзом SmsBat.ua.
<br />Время отправки: %s<br />Ответ сервера: %s',
        'smsbat_message_customer_new_register' => 'Поздравляем с успешной регистрацией в интернет-магазине "{shop_name}"',
        'smsbat_message_customer_new_order' => 'Спасибо за покупку. Ваш номер заказа #{order_id}',
        'smsbat_message_admin_new_customer' => 'Зарегистрирован новый покупатель #{customer_id} {firstname} {lastname}',
        'smsbat_message_admin_new_order' => 'Новый заказ #{order_id}',
        'smsbat_message_admin_new_email' => 'В сапорт отправлено письмо от {email}',
        'smsbat_message_customer_new_order_status' => 'Статус заказа #{order_id} изменился на "{new_status_name}"'
    );
    private $l; // language code (e.g. `ru`)

    public function index()
    {
        $this->l = $this->language->get('code');

        $this->_init();

        $this->data['tab_sel'] = null;
        if ($this->request->server['REQUEST_METHOD'] !== 'POST') {
            $this->_view();
            return;
        }

        if ($this->_validate()) {
            if (((isset($this->request->post['smsbat_login']) && isset($this->request->post['smsbat_password'])) ||
                isset($this->request->post['smsbat_key'])) && empty($this->request->post['is_sms'])
            ){
                unset($this->request->post['smsbat_frmsms_message']);
                unset($this->request->post['smsbat_frmsms_phone']);

                /*
                 * Replace in $_POST array messages templates with language code;
                 */
                foreach ($this->message_keys as $k => $v) {
                    if (!empty($this->request->post[$k])) {
                        $this->request->post[$k . '_' . $this->l] = $this->request->post[$k];
                        unset($this->request->post[$k]);
                    }
                }

                $this->model_setting_setting->editSetting('smsbat', $this->request->post);

                $this->session->data['success'] = $this->language->get('smsbat_saved_success');
                $this->_log->write('['.substr(__FILE__, strlen(DIR_SYSTEM)-1).'] Save settings form form success');
                $this->response->redirect($this->url->link('extension/module/smsbat', 'token='.$this->session->data['token']));

            } else if(!empty($this->request->post['is_sms'])) {
                if (!$this->_gate){
                    $this->_err = $this->language->get('smsbat_error_auth_info');
                } else {
                    $this->_gate->setSign($this->config->get('smsbat_sign'));
                    $this->_gate->sendSms($this->request->post['smsbat_frmsms_phone'],
                        $this->request->post['smsbat_frmsms_message']
                    );

                    $errs = '';
                    if ($errs=$this->_gate->getErrors()) {
                        $this->_err = $errs;
                    } else {
                        $this->session->data['success'] = $this->language->get('smsbat_smssend_success');
                        $this->session->data['success_sms'] = $this->language->get('smsbat_smssend_success');
                        $this->session->data['tab_sel'] = 'tab_sendsms';
                        $this->data['success_sms'] = $this->language->get('smsbat_smssend_success');
                        $this->_log->write('['.substr(__FILE__, strlen(DIR_SYSTEM)-1).'] Send sms from form success');
                        $this->response->redirect(
                            $this->url->link('extension/module/smsbat', 'token='.$this->session->data['token'], 'SSL')
                        );
                    }
                }
            }
        }

        $this->_view();
    }

    private function _breadcrumbs()
    {
        $breadcrumbs[] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token='.$this->session->data['token'], 'SSL'),
            'separator' => false
        );
        $breadcrumbs[] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module/smsbat', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );
        $breadcrumbs[] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/smsbat', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        return $breadcrumbs;
    }

    private function _init()
    {
        require_once(DIR_SYSTEM.'library/smsbat_gateway.php');

        $this->load->model('setting/setting');

        $this->load->model('localisation/language');

        $this->_log = new Log('smsbat.log');

        foreach ($this->load->language('module/smsbat') as $key => $val) {
            $this->data[$key] = $val;
        }

        $settings = $this->model_setting_setting->getSetting('smsbat');

        foreach ($settings as $key => $val) {
            $this->data['frm_'.$key] = $val;
        }

        if (array_key_exists('smsbat_admphone', $settings) && !$settings['smsbat_admphone']){
            $this->data['frm_smsbat_admphone'] = $this->config->get('config_telephone');
        }

        if (!empty($settings)) {
            $this->_gate = new SmsBatGateway(
                $this->data['frm_smsbat_login'],
                $this->data['frm_smsbat_password'],
                $this->data['frm_smsbat_key']
            );
        }

        /*
         * Set templates tab title
         */
        $this->data['smsbat_tab_templates'] = $this->language->get('smsbat_tab_templates');

        /*
         * Set button text into template
         */
        $this->data['smsbat_text_button_save_templates']=$this->language->get('smsbat_text_button_save_templates');

        /*
         * Set template titles to template
         */
        $this->data['smsbat_connection_error_title'] = $this->language->get('smsbat_connection_error_title');
        $this->data['smsbat_customer_new_register_title'] = $this->language
            ->get('smsbat_customer_new_register_title');
        $this->data['smsbat_customer_new_order_title'] = $this->language->get('smsbat_customer_new_order_title');
        $this->data['smsbat_admin_new_customer_title'] = $this->language->get('smsbat_admin_new_customer_title');
        $this->data['smsbat_admin_new_order_title'] = $this->language->get('smsbat_admin_new_order_title');
        $this->data['smsbat_admin_new_email_title'] = $this->language->get('smsbat_admin_new_email_title');
        $this->data['smsbat_customer_new_order_status_title'] = $this->language
            ->get('smsbat_customer_new_order_status_title');

        /*
         * Set templates values into template textareas
         */
        foreach ($this->message_keys as $k => $v) {
            $this->data[$k] = strlen($this->config->get($k . '_' . $this->l)) > 0 ?
                $this->config->get($k . '_' . $this->l) : $v;
        }
    }

    protected function _view()
    {
        $this->document->setTitle($this->language->get('heading_title'));

        # Set variables for view file
        $this->data['module_version'] = SmsBatGateway::VERSION;
        $this->data['err']            = $this->_err;
        $this->data['breadcrumbs']    = $this->_breadcrumbs();

        $this->data['languages']      = $this->model_localisation_language->getLanguages();

        $this->data['action'] = $this->url->link('extension/module/smsbat', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL');

        # Save a new form values from request
        foreach ($this->request->post as $key => $value) {
            $this->data['frm_' . $key] = $value;
        }

        if (isset($this->session->data['success'])){
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        }

        if (isset($this->session->data['success_sms'])){
            $this->data['success_sms'] = $this->session->data['success_sms'];
            unset($this->session->data['success_sms']);
        }

        if (isset($this->session->data['tab_sel'])){
            $this->data['tab_sel'] = $this->session->data['tab_sel'];
            unset($this->session->data['tab_sel']);
        }

        # Template rendering
//    $this->children = array('common/header', 'common/footer');

        $this->data['header'] = $this->load->controller('common/header');
        $this->data['column_left'] = $this->load->controller('common/column_left');
        $this->data['footer'] = $this->load->controller('common/footer');

        $this->template = 'module/smsbat.tpl';


        $this->response->setOutput($this->load->view('module/smsbat.tpl', $this->data));

    }

    private function _validate()
    {
        if (!$this->user->hasPermission('modify', 'extension/module/smsbat')) {
            $this->_err = $this->language->get('smsbat_error_permission');
            return false;
        }

        if (empty($this->request->post['smsbat_login']) and empty($this->request->post['smsbat_key'])) {
            $this->_err = $this->language->get('smsbat_error_login_field');
            return false;
        }

        if (empty($this->request->post['smsbat_password'])  and empty($this->request->post['smsbat_key'])) {
            $this->_err = $this->language->get('smsbat_error_password_field');
            return false;
        }

        if (!empty($this->request->post['is_sms'])){

            $this->data['tab_sel'] = 'tab_sendsms';
            if (empty($this->request->post['smsbat_frmsms_message'])
                || !preg_match("!\+[0-9]{10,14}!si", $this->request->post['smsbat_frmsms_phone'])
            ) {
                //$this->_err = $this->language->get('smsbat_error_phone');
                $this->_err = ' ';
                return false;
            }
        }

        if (empty($this->request->post['smsbat_sign'])) {
            $this->_err = $this->language->get('smsbat_error_sign_field');
            return false;
        } else if (strlen($this->request->post['smsbat_sign'])>11) {
            $this->_err = $this->language->get('smsbat_error_sign_to_large');
            return false;
        }

        if (empty($this->request->post['smsbat_admphone'])) {
            $this->_err = $this->language->get('smsbat_error_admphone_field');
            return false;
        }


        try{
            // Test connection
            $gateway = new SmsBatGateway(
                $this->request->post['smsbat_login'],
                $this->request->post['smsbat_password'],
                $this->request->post['smsbat_key']
            );

            if (!$gateway->testConnection()){
                $this->_err = 'Connection test failed';
                $this->_err .= '<br/>HTTP/HTTPS errors:<br/>'.$gateway->getErrors('<br/>');
                return false;
            }
        } catch(Exception $ax) {
            return false;
        }


        return true;
    }

}
