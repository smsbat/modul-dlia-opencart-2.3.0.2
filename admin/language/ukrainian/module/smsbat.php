<?php

// Heading
$_['heading_title'] = 'SmsBat.com';
$_['text_module']   = 'Модулі';
$_['text_edit']   = 'Редагувати модуль SmsBat.com';

$_['smsbat_saved_success'] = 'Успішно збережено налаштування';
$_['smsbat_smssend_success'] = 'Повідомлення успішно надіслано на шлюз';

// Error
$_['smsbat_error_permission'] = 'Ви не маєте повноважень для зміни налаштувань у модулі!';
$_['smsbat_error_request'] = 'Помилка запиту';
$_['smsbat_error_auth_info'] = 'Необхідно спочатку вказаити ідентифікаційні дані SMS-шлюза';
$_['smsbat_error_login_field'] = 'Необхідно вказати логін';
$_['smsbat_error_password_field'] = 'Необхідно вказати пароль';
$_['smsbat_error_sign_field'] = 'Необходимо підпис для повідомлень';
$_['smsbat_error_admphone_field'] = 'Вкажіть номер телефону адміністратора';
$_['smsbat_error_sign_to_large'] = 'Підпис занадто довга. Максимум 11 символів.';
$_['smsbat_error_empty_frmsms_message'] = 'Необхідно вказати текст повідомлення.';
$_['smsbat_error_frmsms'] = 'Помилка з надсиланням повідомлення';

// Tabs name in view
$_['smsbat_tab_connection'] = 'Налаштування шлюза';
$_['smsbat_tab_signature'] = 'Підпис для повідомлень';
$_['smsbat_tab_events'] = 'Виконувати для дій';
$_['smsbat_tab_about'] = 'Про модуль';
$_['smsbat_tab_sendsms'] = 'Надістали SMS';

// Text messges
$_['smsbat_text_gate_settings'] = 'Налаштування шлюзу';
$_['smsbat_text_login'] = 'Логін';
$_['smsbat_text_login_placeholder'] = 'Логін (телефон) з сайту SmsBat.com';
$_['smsbat_error_login'] = 'Порожній телефон, чи не відповідає формату. Повинен бути на зразок такого: +380112223344';
$_['smsbat_text_password'] = 'Пароль';
$_['smsbat_error_password'] = 'Пароль не може бути порожнім !';
$_['smsbat_text_key'] = 'API ключ';
$_['smsbat_error_key'] = 'Порожній API ключ';
$_['smsbat_text_sign'] = 'Підпис повідомлень';
$_['smsbat_error_sign'] = 'Порожній підпис, або він не відповідає формату. Повинен бути не довшим 11 символів латиницею !';
$_['smsbat_text_admphone'] = 'Телефон адміністратора';
$_['smsbat_error_admphone'] = 'Порожній телефон адміністратора, чи не відповідає формату. Повинен бути схожим на +380112223344';
$_['smsbat_text_phone'] = 'Телефон отримувача';
$_['smsbat_error_phone'] = 'Порожній телефон отримувача, чи не відповідає формату. Зробіть його схожим на +380112223344';
$_['smsbat_text_notify_sms_to_admin'] = 'Повідомляти про події адміністратора';
$_['smsbat_text_notify_sms_to_customer'] = 'Повідомляти про події покупця';
$_['smsbat_text_connection_established'] = 'З`єднання з SMS-шлюзом встановлено';
$_['smsbat_text_connection_error'] = 'Відсутній зв`язок із SMS-шлюзом';
$_['smsbat_events_admin_new_customer'] = 'Новий покупець зареєструвався';
$_['smsbat_events_admin_new_order'] = 'Здійснили нове замовлення';
$_['smsbat_events_admin_new_email'] = 'Надійшов лист з контактної форми магазину';
$_['smsbat_text_frmsms_message'] = 'Текст повідомлення';
$_['smsbat_error_message'] = 'Порожній текст повідомлення';
$_['smsbat_text_frmsms_phone'] = 'Номер отримувача';
$_['smsbat_text_button_send_sms'] = 'Надіслати SMS';
$_['smsbat_events_admin_gateway_connection_error'] = 'Повідомляти на email у випадках неполадок з`єднання із шлюзом';
$_['smsbat_events_customer_new_order_status'] = 'Зміна статусу замовлення';
$_['smsbat_events_customer_new_order'] = 'Повідомляти покупця про нове замовлення';
$_['smsbat_events_customer_new_register'] = 'Вдале завершення реєтрації';

$_['smsbat_message_customer_new_order_status'] = 'Статус замовлення #{order_id} змінено на "{new_status_name}"';

$_['smsbat_text_connection_tab_description'] =
    'Вкажіть вірні дані для подключения до шлюзу SmsBat.com через HTTP/HTTPS протокол.<br/>';

$_['smsbat_text_about_tab_description'] =
    '<b>%s &copy; %s Всі права захищено</b><br/>
<br/>
Модуль призначено для розсилки SMS повідомлень за допомогою шлюза SmsBat.com.
<br/><br/>
Дана робота розповсюджується на основі ліцензії BSD<br/><br/>
Поточна версія: %s<br/>';

// UPD from 2016-08-04

$_['smsbat_tab_templates'] = 'Шаблони повідомлень';
$_['smsbat_connection_error_title'] = "Помилка з'єднання зі шлюзом";
$_['smsbat_customer_new_register_title'] = 'Привітання користувача з реєстрацією';
$_['smsbat_customer_new_order_title'] = 'Повідомлення про покупку';
$_['smsbat_admin_new_customer_title'] = 'Повідомлення адміну про нового користувача';
$_['smsbat_admin_new_order_title'] = 'Повідомлення адміну про нове замовлення';
$_['smsbat_admin_new_email_title'] = 'Повідомлення адміну про запит в службу підтримки';
$_['smsbat_customer_new_order_status_title'] = 'Повідомлення про зміну статуса замовлення';
$_['smsbat_text_button_save_templates'] = 'Зберегти шаблони повідомлення';



// messages templates variables :

$_['smsbat_variables_notice'] = 'Користувач може заповнити не всі поля в формі и тоді декотрі зі змінних в SMS будуть заменені пустими значеннями';
$_['smsbat_variable_shop_name'] = 'Назва магазину';
$_['smsbat_variable_customer_id'] = '№ користувача';
$_['smsbat_variable_firstname'] = 'Им&#39;я';
$_['smsbat_variable_lastname'] = 'Прізвище';
$_['smsbat_variable_email'] = 'Email';
$_['smsbat_variable_telephone'] = 'Телефон';
$_['smsbat_variable_fax'] = 'Факс';
$_['smsbat_variable_company'] = 'Назва компанії';
$_['smsbat_variable_address_1'] = 'Адреса 1';
$_['smsbat_variable_address_2'] = 'Адреса 2';
$_['smsbat_variable_city'] = 'Місто';
$_['smsbat_variable_postcode'] = 'Поштовый код';
$_['smsbat_variable_password'] = 'Пароль';
$_['smsbat_variable_ip'] = 'IP';
$_['smsbat_variable_order_id'] = '№ замовлення';
$_['smsbat_variable_store_url'] = 'URL магазину';
$_['smsbat_variable_custom_field'] = 'Користувацьке поле';
$_['smsbat_variable_payment_method'] = 'Метод оплати';
$_['smsbat_variable_shipping_address_1'] = 'Адреса доставки 1';
$_['smsbat_variable_shipping_address_2'] = 'Адреса доставки 2';
$_['smsbat_variable_shipping_postcode'] = 'Поштовый код доставки';
$_['smsbat_variable_shipping_city'] = 'Місто доставки';
$_['smsbat_variable_shipping_region'] = 'Область доставки';
$_['smsbat_variable_shipping_country'] = 'Країна доставки';
$_['smsbat_variable_shipping_method'] = 'Спосіб доставки';
$_['smsbat_variable_comment'] = 'Коментар';
$_['smsbat_variable_total'] = 'Сума';
$_['smsbat_variable_currency_code'] = 'Код валюти';
$_['smsbat_variable_date_added'] = 'Дата додавання';
$_['smsbat_variable_name'] = 'Им&#39;я';
$_['smsbat_variable_enquiry'] = 'Текст запиту в сапорт';
$_['smsbat_variable_new_status_name'] = 'Назва нового статусу';
$_['smsbat_variable_date_modified'] = 'Дата зміни';
