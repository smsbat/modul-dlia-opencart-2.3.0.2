<?php

// Heading
$_['heading_title'] = 'SmsBat.com';
$_['text_module']   = 'Modules';
$_['text_edit']   = 'Edit module SmsBat.com';

$_['smsbat_saved_success'] = 'Success saved settings';
$_['smsbat_smssend_success'] = 'Sucess sended sms to gatawey';

// Error
$_['smsbat_error_permission'] = 'You have not authority to change settings of this module!';
$_['smsbat_error_request'] = 'Request faild';
$_['smsbat_error_auth_info'] = 'You must set authorize setting to SMS-Gate';
$_['smsbat_error_login_field'] = 'You must specify a login';
$_['smsbat_error_password_field'] = 'You must specify a password';
$_['smsbat_error_sign_field'] = 'You must specify a signature';
$_['smsbat_error_admphone_field'] = 'You must set admin phone';
$_['smsbat_error_sign_to_large'] = 'Signature is to large. Maximum 11 symbols';
$_['smsbat_error_empty_frmsms_message'] = 'You must specify a text of message';
$_['smsbat_error_frmsms'] = 'Error with sending a message';

// Tabs name in view
$_['smsbat_tab_connection'] = 'Gatawey settings';
$_['smsbat_tab_signature'] = 'Signature';
$_['smsbat_tab_events'] = 'Execute on events';
$_['smsbat_tab_about'] = 'About';
$_['smsbat_tab_sendsms'] = 'Send SMS';

// Text messges
$_['smsbat_text_gate_settings'] = 'Gate settings';
$_['smsbat_text_login'] = 'Login';
$_['smsbat_text_login_placeholder'] = 'SmsBat.com login (phone)';
$_['smsbat_error_login'] = 'Empty phone or wrong format. Must be e.g. +380112223344';
$_['smsbat_text_password'] = 'Password';
$_['smsbat_error_password'] = 'Password must be not empty !';
$_['smsbat_text_key'] = 'API Key';
$_['smsbat_error_key'] = 'Empty API Key';
$_['smsbat_text_sign'] = 'Signature';
$_['smsbat_error_sign'] = 'Empty signature or wrong format. Must be no longer 11 chars !';
$_['smsbat_text_admphone'] = 'Admin phone';
$_['smsbat_error_admphone'] = 'Empty administrator phone or wrong format. Must be e.g. +380112223344';
$_['smsbat_text_phone'] = 'Recipient phone';
$_['smsbat_error_phone'] = 'Empty phone or wrong format. Must be e.g. +380112223344';
$_['smsbat_text_notify_sms_to_admin'] = 'Notify evens to admin';
$_['smsbat_text_notify_sms_to_customer'] = 'Notify evens to customer';
$_['smsbat_text_connection_established'] = 'Connection to gatawey is established';
$_['smsbat_text_connection_error'] = 'Gateway is not connected';
$_['smsbat_events_admin_new_customer'] = 'The new customer is registered';
$_['smsbat_events_admin_new_order'] = 'A new order is implemented';
$_['smsbat_events_admin_new_email'] = 'Received new email with store contact form';
$_['smsbat_text_frmsms_message'] = 'Text of message';
$_['smsbat_error_message'] = 'Empty message';
$_['smsbat_text_frmsms_phone'] = 'Destionation phone';
$_['smsbat_text_button_send_sms'] = 'Send SMS';
$_['smsbat_events_admin_gateway_connection_error'] = 'Notify me by email at the gateway connection fails';
$_['smsbat_events_customer_new_order_status'] = 'Change the order status';
$_['smsbat_events_customer_new_order'] = 'The buyer a message about a new order';
$_['smsbat_events_customer_new_register'] = 'The registration is completed successfully';

$_['smsbat_message_customer_new_order_status'] = 'Order status #{order_id} is changed on "{new_status_name}".';

$_['smsbat_text_connection_tab_description'] =
'Enter the correct information to connect to gateway SmsBat.com via HTTP/HTTPS protocol.<br/>';

$_['smsbat_text_about_tab_description'] =
'<b>%s &copy; %s All rights reserved</b><br />
<br/>
Module is designed to send SMS notifications via gateway SmsBat.com.
<br/><br/>
This product is distributed under a BSD License<br/><br/>
Current version: %s<br />';

// UPD from 2016-08-04

$_['smsbat_tab_templates'] = 'Messages templates';
$_['smsbat_connection_error_title'] = 'Error with gateway connection';
$_['smsbat_customer_new_register_title'] = 'Congratulations to the user registration';
$_['smsbat_customer_new_order_title'] = 'Post purchase';
$_['smsbat_admin_new_customer_title'] = 'Notification to admin about new user';
$_['smsbat_admin_new_order_title'] = 'Notification to admin about new order';
$_['smsbat_admin_new_email_title'] = 'Notification to admin about message to support';
$_['smsbat_customer_new_order_status_title'] = 'Notice of change of order status';
$_['smsbat_text_button_save_templates'] = 'To save messages templates';


// Messages templates variables:

$_['smsbat_variables_notice'] = 'You can not fill out all the fields in the form, and then some of the variables in the SMS will be replaced with empty values';
$_['smsbat_variable_shop_name'] = 'Store Name';
$_['smsbat_variable_customer_id'] = 'user number';
$_['smsbat_variable_firstname'] = 'Name';
$_['smsbat_variable_lastname'] = 'Name';
$_['smsbat_variable_email'] = 'Email';
$_['smsbat_variable_telephone'] = 'Telephone';
$_['smsbat_variable_fax'] = 'Fax';
$_['smsbat_variable_company'] = 'Company';
$_['smsbat_variable_address_1'] = 'Address 1';
$_['smsbat_variable_address_2'] = 'Address 2';
$_['smsbat_variable_city'] = 'City';
$_['smsbat_variable_postcode'] = 'Post Code';
$_['smsbat_variable_password'] = 'Password';
$_['smsbat_variable_ip'] = 'IP';
$_['smsbat_variable_order_id'] = 'Order number';
$_['smsbat_variable_store_url'] = 'URL store';
$_['smsbat_variable_custom_field'] = 'Custom Field';
$_['smsbat_variable_payment_method'] = 'Payment method';
$_['smsbat_variable_shipping_address_1'] = 'Shipping Address 1';
$_['smsbat_variable_shipping_address_2'] = 'Shipping Address 2';
$_['smsbat_variable_shipping_postcode'] = 'mail service code';
$_['smsbat_variable_shipping_city'] = 'City of delivery';
$_['smsbat_variable_shipping_region'] = 'Region / Delivery Area';
$_['smsbat_variable_shipping_country'] = 'Delivery Country';
$_['smsbat_variable_shipping_method'] = 'Delivery method';
$_['smsbat_variable_comment'] = 'Comments';
$_['smsbat_variable_total'] = 'Sum';
$_['smsbat_variable_currency_code'] = 'Currency code';
$_['smsbat_variable_date_added'] = 'Date';
$_['smsbat_variable_name'] = 'Name';
$_['smsbat_variable_enquiry'] = 'text query / question in the Support';
$_['smsbat_variable_new_status_name'] = 'The name of the new status';
$_['smsbat_variable_date_modified'] = 'Date Modified';
