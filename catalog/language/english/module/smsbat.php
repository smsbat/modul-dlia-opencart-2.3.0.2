<?php

$_['smsbat_saved_success'] = 'Success saved settings';

$_['smsbat_message_connection_error'] = 'When you send an alert via SMS could not string any Gateway.<br />Sending time: %s<br />Server response: %s';
$_['smsbat_message_customer_new_register'] = 'Congratulations on your successful registration in the store "{shop_name}".';
$_['smsbat_message_customer_new_order'] = 'Thank you for your purchase. Your order number #{order_id}';
$_['smsbat_message_admin_new_order'] = 'A new order is performed #{order_id}';
$_['smsbat_message_admin_new_customer'] = 'The new customer is registered #{customer_id} {firstname} {lastname}';
$_['smsbat_message_admin_new_email'] = 'It is sent a new mail to support from {email}';

$_['smsbat_page_head_title'] = 'SmsBat module';

$_['smsbat_message_customer_new_order_status'] = 'Order status #{order_id} is changed on "{new_status_name}".';
