<?php

$_['smsbat_saved_success'] = 'Налашутвання успішно збережено';

$_['smsbat_message_connection_error'] = 'Виникли неполадки зі шлюзом SmsBat під час пересилання SMS повідомлення.<br />Час відправки: %s<br />Відповідь серверу: %s';
$_['smsbat_message_customer_new_register'] = 'Вітаємо з успішною реєстрацією в Інтернет-магазині "{shop_name}"';
$_['smsbat_message_customer_new_order'] = 'Дякуюємо за покупку. Ваш номер замовлення #{order_id}';
$_['smsbat_message_admin_new_customer'] = 'Зареєструвався новий покупець #{customer_id} {firstname} {lastname}';
$_['smsbat_message_admin_new_order'] = 'Нове замовлення #{order_id}';
$_['smsbat_message_admin_new_email'] = 'В сапорт надіслано лита від {email}';

$_['smsbat_page_head_title'] = 'Модуль SmsBat';

$_['smsbat_message_customer_new_order_status'] = 'Статус замовлення #{order_id} змінився на "{new_status_name}".';
