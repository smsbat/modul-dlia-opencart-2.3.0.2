<?php

$_['smsbat_saved_success'] = 'Настройки удачно сохранены';

$_['smsbat_message_connection_error'] = 'При отправке оповещения по SMS возникли неполадки со шлюзом SmsBat.com.<br />Время отправки: %s<br />Ответ сервера: %s';
$_['smsbat_message_customer_new_register'] = 'Поздравляем с успешной регистрацией в Интернет-магазине "{shop_name}"';
$_['smsbat_message_customer_new_order'] = 'Спасибо за покупку. Ваш номер заказа #{order_id}';
$_['smsbat_message_admin_new_customer'] = 'Зарегистрирован новый покупатель #{customer_id} {firstname} {lastname}';
$_['smsbat_message_admin_new_order'] = 'Новый заказ #{order_id}';
$_['smsbat_message_admin_new_email'] = 'В сапорт отправлено письмо от {email}';

$_['smsbat_page_head_title'] = 'Модуль SmsBat';

$_['smsbat_message_customer_new_order_status'] = 'Статус заказа #{order_id} изменился на "{new_status_name}"';
