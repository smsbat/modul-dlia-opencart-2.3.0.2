<?php
class ControllerCheckoutSuccess extends Controller {
	public function index() {
		$this->load->language('checkout/success');

		if (isset($this->session->data['order_id'])) {

            if ($this->smsbat_init()==true && $this->config->get('smsbat_events_admin_new_order')) {

                $this->load->model('checkout/order');
                $last_order = $this->model_checkout_order->getOrder($this->session->data['order_id']);

                $l = $this->language->get('code');
                $message = $this->config->get('smsbat_message_admin_new_order_' . $l);
                if (empty($message))
                    $message = $this->language->get('smsbat_message_admin_new_order');



                // an array of templates variables
                $_replacement_arr = array(
                    'shop_name' => !empty($last_order['store_name']) ? $last_order['store_name'] : '',
                    'order_id' => !empty($last_order['order_id']) ? $last_order['order_id'] : '',
                    'store_url' => !empty($last_order['store_url']) ? $last_order['store_url'] : '',
                    'customer_id' => !empty($last_order['customer_id']) ? $last_order['customer_id'] : '',
                    'firstname' => !empty($last_order['firstname']) ? $last_order['firstname'] : '',
                    'lastname' => !empty($last_order['lastname']) ? $last_order['lastname'] : '',
                    'email' => !empty($last_order['email']) ? $last_order['email'] : '',
                    'telephone' => !empty($last_order['telephone']) ? $last_order['telephone'] : '',
                    'custom_field' => !empty($last_order['custom_field']) ? $last_order['custom_field'] : '',
                    'payment_method' => !empty($last_order['payment_method']) ? $last_order['payment_method'] : '',
                    'shipping_address_1' => !empty($last_order['shipping_address_1']) ? $last_order['shipping_address_1'] : '',
                    'shipping_address_2' => !empty($last_order['shipping_address_2']) ? $last_order['shipping_address_2'] : '',
                    'shipping_postcode' => !empty($last_order['shipping_postcode']) ? $last_order['shipping_postcode'] : '',
                    'shipping_city' => !empty($last_order['shipping_city']) ? $last_order['shipping_city'] : '',
                    'shipping_region' => !empty($last_order['shipping_zone']) ? $last_order['shipping_zone'] : '',
                    'shipping_country' => !empty($last_order['shipping_country']) ? $last_order['shipping_country'] : '',
                    'shipping_method' => !empty($last_order['shipping_method']) ? $last_order['shipping_method'] : '',
                    'comment' => !empty($last_order['comment']) ? $last_order['comment'] : '',
                    'total' => !empty($last_order['total']) ? number_format(floatval($last_order['total']), 1,'.','') : '',
                    'currency_code' => !empty($last_order['currency_code']) ? $last_order['currency_code'] : '',
                    'ip' => !empty($last_order['ip']) ? $last_order['ip'] : '',
                    'date_added' => !empty($last_order['date_added']) ? $last_order['date_added'] : '',
                );

                // Processing replacement a tembpaltes variables
                foreach ($_replacement_arr as $k => $v) {
                    $message = str_replace('{' . $k . '}', $v, $message);
                }

                // Replacement all customer variables that wasn't replaced on previous step
                $message = preg_replace("/\{[a-z0\_]\}/si", '', $message);

                $message = trim($message);




                $phones = array($this->config->get('smsbat_admphone'));
                if (strlen($this->config->get('smsbat_admphone1'))) $phones[] = $this->config->get('smsbat_admphone1');

                $this->smsbat_logger->write('['.substr(__FILE__, strlen(DIR_SYSTEM)-1).
                    '] Event: smsbat_events_admin_new_order. Dest phone:'
                    .implode(', ', $phones) ." Message: ".$message
                );

                foreach($phones as $phone)
                    $this->smsbat_gateway->sendSms($phone, $message);
            }

            if ($this->config->get('smsbat_events_customer_new_order')){
                $this->load->model('checkout/order');
                $last_order = $this->model_checkout_order->getOrder($this->session->data['order_id']);

                $l = $this->language->get('code');
                $message_text = $this->config->get('smsbat_message_customer_new_order_' . $l);
                if (empty($message_text))
                    $message_text = $this->language->get('smsbat_message_customer_new_order');

                $message = sprintf(
                    $message_text,
                    $this->session->data['order_id']
                );


                // an array of templates variables
                $_replacement_arr = array(
                    'shop_name' => !empty($last_order['store_name']) ? $last_order['store_name'] : '',
                    'order_id' => !empty($last_order['order_id']) ? $last_order['order_id'] : '',
                    'store_url' => !empty($last_order['store_url']) ? $last_order['store_url'] : '',
                    'customer_id' => !empty($last_order['customer_id']) ? $last_order['customer_id'] : '',
                    'firstname' => !empty($last_order['firstname']) ? $last_order['firstname'] : '',
                    'lastname' => !empty($last_order['lastname']) ? $last_order['lastname'] : '',
                    'email' => !empty($last_order['email']) ? $last_order['email'] : '',
                    'telephone' => !empty($last_order['telephone']) ? $last_order['telephone'] : '',
                    'custom_field' => !empty($last_order['custom_field']) ? $last_order['custom_field'] : '',
                    'payment_method' => !empty($last_order['payment_method']) ? $last_order['payment_method'] : '',
                    'shipping_address_1' => !empty($last_order['shipping_address_1']) ? $last_order['shipping_address_1'] : '',
                    'shipping_address_2' => !empty($last_order['shipping_address_2']) ? $last_order['shipping_address_2'] : '',
                    'shipping_postcode' => !empty($last_order['shipping_postcode']) ? $last_order['shipping_postcode'] : '',
                    'shipping_city' => !empty($last_order['shipping_city']) ? $last_order['shipping_city'] : '',
                    'shipping_region' => !empty($last_order['shipping_zone']) ? $last_order['shipping_zone'] : '',
                    'shipping_country' => !empty($last_order['shipping_country']) ? $last_order['shipping_country'] : '',
                    'shipping_method' => !empty($last_order['shipping_method']) ? $last_order['shipping_method'] : '',
                    'comment' => !empty($last_order['comment']) ? $last_order['comment'] : '',
                    'total' => !empty($last_order['total']) ? number_format(floatval($last_order['total']), 1,'.','') : '',
                    'currency_code' => !empty($last_order['currency_code']) ? $last_order['currency_code'] : '',
                    'ip' => !empty($last_order['ip']) ? $last_order['ip'] : '',
                    'date_added' => !empty($last_order['date_added']) ? $last_order['date_added'] : '',
                );

                // Processing replacement a tembpaltes variables
                foreach ($_replacement_arr as $k => $v) {
                    $message = str_replace('{' . $k . '}', $v, $message);
                }

                // Replacement all customer variables that wasn't replaced on previous step
                $message = preg_replace("/\{[a-z0\_]\}/si", '', $message);

                $message = trim($message);


                $this->smsbat_logger->write('['.substr(__FILE__, strlen(DIR_SYSTEM)-1).
                    '] Event: smsbat_events_customer_new_order. Dest phone:'
                    .$last_order['telephone'] ." Message: ".$message
                );

                $this->smsbat_gateway->sendSms($last_order['telephone'], $message);
            }

            $this->cart->clear();

			// Add to activity log
			if ($this->config->get('config_customer_activity')) {
				$this->load->model('account/activity');

				if ($this->customer->isLogged()) {
					$activity_data = array(
						'customer_id' => $this->customer->getId(),
						'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
						'order_id'    => $this->session->data['order_id']
					);

					$this->model_account_activity->addActivity('order_account', $activity_data);
				} else {
					$activity_data = array(
						'name'     => $this->session->data['guest']['firstname'] . ' ' . $this->session->data['guest']['lastname'],
						'order_id' => $this->session->data['order_id']
					);

					$this->model_account_activity->addActivity('order_guest', $activity_data);
				}
			}

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['totals']);
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_basket'),
			'href' => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_checkout'),
			'href' => $this->url->link('checkout/checkout', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_success'),
			'href' => $this->url->link('checkout/success')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		if ($this->customer->isLogged()) {
			$data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', true), $this->url->link('account/order', '', true), $this->url->link('account/download', '', true), $this->url->link('information/contact'));
		} else {
			$data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
		}

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/success', $data));
	}
}